package org.bookstore.api.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.function.Consumer;

public interface Event<EVENT extends Event<EVENT>> extends Serializable {

  @FunctionalInterface
  interface Handler<EVENT extends Record & Event<EVENT>> extends Consumer<EVENT> {}

  record DomainEvent<EVENT extends Record & Event<EVENT>>(UUID id, String model, String identity, int version, String name, EVENT event, LocalDateTime timestamp) implements Event<EVENT> {
    public DomainEvent(String model, String identity, EVENT event) {
      this(UUID.randomUUID(), model, identity, 0, event.getClass().getSimpleName(), event, LocalDateTime.now());
    }
  }
}
