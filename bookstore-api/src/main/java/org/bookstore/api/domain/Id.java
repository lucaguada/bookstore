package org.bookstore.api.domain;

@FunctionalInterface
public interface Id<TYPE> extends Value<TYPE> {}
