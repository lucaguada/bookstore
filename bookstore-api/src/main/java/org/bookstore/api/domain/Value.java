package org.bookstore.api.domain;

import java.util.function.Supplier;

@FunctionalInterface
public interface Value<OBJECT> extends Supplier<OBJECT> {
  @Override
  default OBJECT get() { return value(); }

  OBJECT value();
}
