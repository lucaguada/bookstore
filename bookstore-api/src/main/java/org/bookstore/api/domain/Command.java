package org.bookstore.api.domain;

import java.io.Serializable;
import java.util.concurrent.Future;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

public interface Command<COMMAND extends Command<COMMAND>> extends Serializable {
  @FunctionalInterface
  interface Handler<AGGREGATE extends Aggregate<?>, COMMAND extends Record & Command<COMMAND>, EVENT extends Record & Event<EVENT>> extends BiFunction<AGGREGATE, COMMAND, Future<EVENT>> {}
}
