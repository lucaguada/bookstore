package org.bookstore.api.domain;

public interface Aggregate<ID extends Record & Id<?>> {
  ID id();
}
