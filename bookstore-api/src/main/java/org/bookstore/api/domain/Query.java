package org.bookstore.api.domain;

import java.io.Serializable;
import java.util.concurrent.Future;
import java.util.function.Function;

public interface Query<QUERY extends Query<QUERY>> extends Serializable {
  @FunctionalInterface
  interface Handler<QUERY extends Record & Query<QUERY>, VIEW extends Record & View<VIEW>> extends Function<QUERY, Future<VIEW>> {}
}
