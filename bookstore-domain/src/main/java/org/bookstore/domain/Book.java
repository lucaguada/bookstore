package org.bookstore.domain;

import org.bookstore.api.domain.Aggregate;
import org.bookstore.domain.book.Initial;
import org.bookstore.domain.book.command.Register;
import org.bookstore.domain.book.event.Registered;
import org.bookstore.domain.book.state.Unregistered;

import java.util.Optional;
import java.util.UUID;

public sealed interface Book extends Aggregate<Book.Id> permits Initial, Unregistered {

  record Id(UUID value) implements org.bookstore.api.domain.Id<UUID> {
    static Id random() {
      return new Id(UUID.randomUUID());
    }
    static Optional<Id> from(String value) {
      return Optional.ofNullable(value)
        .map(it -> {
          try {
            return UUID.fromString(value);
          } catch (Exception e) {
            return null;
          }
        })
        .map(Id::new);
    }
  }

  sealed interface Command<COMMAND extends Record & Book.Command<COMMAND>> extends org.bookstore.api.domain.Command<COMMAND> permits Register {}
  sealed interface Event<EVENT extends Record & Book.Event<EVENT>> extends org.bookstore.api.domain.Event<EVENT> permits Registered {}
}
