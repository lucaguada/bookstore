package org.bookstore.domain.value;

import org.bookstore.api.domain.Value;

import java.util.Optional;

public record Description(String value) implements Value<String> {
  public Description {
    assert value != null && value.length() >= 3 && value.length() < 400;
  }

  static Optional<Description> of(String value) {
    return Optional.ofNullable(value)
      .filter(it -> value.length() >= 3 && value.length() < 4000)
      .map(Description::new);
  }
}
