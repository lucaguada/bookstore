package org.bookstore.domain.value;

import org.bookstore.api.domain.Value;

import java.util.Optional;

public record Title(String value) implements Value<String> {
  public Title {
    assert value != null && value.length() >= 3 && value.length() < 255;
  }

  static Optional<Title> of(String value) {
    return Optional.ofNullable(value)
      .filter(it -> value.length() >= 3 && value.length() < 255)
      .map(Title::new);
  }
}
