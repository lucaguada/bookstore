package org.bookstore.domain.book.event;

import org.bookstore.domain.Book;
import org.bookstore.domain.value.Description;
import org.bookstore.domain.value.Title;

public record Registered(Title title, Description description) implements Book.Event<Registered> {
}
