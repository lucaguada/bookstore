package org.bookstore.domain.book.command;

import org.bookstore.domain.Book;
import org.bookstore.domain.value.Description;
import org.bookstore.domain.value.Title;

public record Register(Title title, Description description) implements Book.Command<Register> {
}
