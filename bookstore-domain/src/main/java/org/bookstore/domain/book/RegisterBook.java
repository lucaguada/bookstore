package org.bookstore.domain.book;

import org.bookstore.api.domain.Command;
import org.bookstore.domain.Book;
import org.bookstore.domain.book.command.Register;
import org.bookstore.domain.book.event.Registered;
import org.bookstore.domain.book.state.Unregistered;

import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

public record RegisterBook() implements Command.Handler<Book, Register, Registered> {
  @Override
  public Future<Registered> apply(Book book, Register register) {
    return switch (book) {
      case Unregistered(Book.Id id) when id.value() != null -> new FutureTask<>(() -> new Registered(register.title(), register.description()));
      default -> throw new IllegalStateException();
    };
  }
}
