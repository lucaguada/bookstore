package org.bookstore.domain.book.state;

import org.bookstore.domain.Book;

public record Unregistered(Id id) implements Book {}
